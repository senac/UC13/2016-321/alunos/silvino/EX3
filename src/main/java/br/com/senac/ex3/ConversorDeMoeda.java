
package br.com.senac.ex3;

public class ConversorDeMoeda {
    
    public static final double DOLAR = 3;
    public static final double AUSTRALIANO = 2;
    public static final double REAL = 1;

    private double cambiada;
    private double moeda;
    private double resultado;

    public ConversorDeMoeda() {
    }

    public ConversorDeMoeda(double cambiada, double moeda) {
        this.cambiada = cambiada;
        this.moeda = moeda;
    }
    

    public double calcular(Cambio cambio, double ValorOrigem, double ValorResultado) {

       

        if(cambiada == REAL && moeda != AUSTRALIANO){
            resultado = cambio.getReal()/DOLAR;
        }
        if(cambiada == REAL && moeda != DOLAR){
            resultado = cambio.getReal()/AUSTRALIANO;
            
        }
        
       if(cambiada == AUSTRALIANO && moeda != DOLAR){
            resultado = cambio.getAustraliano()*REAL;
            
        }
       if(cambiada == AUSTRALIANO && moeda != REAL){
            resultado = cambio.getAustraliano()/DOLAR;
                        
        }
       if(cambiada == DOLAR && moeda != AUSTRALIANO){
            resultado = cambio.getDolar()*REAL;
            
        }
       if(cambiada == DOLAR && moeda != REAL){
            resultado = cambio.getDolar()*AUSTRALIANO;
            
        }
       
        
        return resultado;
    }

    
}
