package br.com.senac.ex3.Test;

import br.com.senac.ex3.Cambio;
import br.com.senac.ex3.ConversorDeMoeda;
import org.junit.Assert;
import org.junit.Test;

public class ConversorDeMoedaTeste {

    @Test
    public void
            deveConverterDeRealParaDolar() {
        Cambio cambio = new Cambio();
        ConversorDeMoeda conversor = new ConversorDeMoeda();
        double resultado = conversor.calcular(cambio, 1, 1);

        Assert.assertEquals(2.5, resultado, 0.001);

    }

    
}

